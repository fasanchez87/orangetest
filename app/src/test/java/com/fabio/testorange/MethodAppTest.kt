package com.fabio.testorange

import com.fabio.testorange.app.TestOrangeApp.Companion.fixFee
import com.fabio.testorange.app.TestOrangeApp.Companion.isNegativeAmount
import com.fabio.testorange.app.TestOrangeApp.Companion.validateAmount
import com.fabio.testorange.app.TestOrangeApp.Companion.validateText
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MethodAppTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    //PRUEBAS UNITARIAS METODOS SUSCEPTIBLES A FALLOS
    @Test
    fun testMethodValidateTextEmpty(){
       assertEquals("Not value.", validateText(null))
    }

    @Test
    fun testMethodValidateAmount(){
        assertEquals("Ingreso", validateAmount(3F))
    }

    @Test
    fun testMethodNegativeAmount(){
        assertEquals(true, isNegativeAmount(-3F))
    }

    @Test
    fun testMethodFixFee(){
        assertEquals(-36F, fixFee(-33F, -3F))
    }

}
