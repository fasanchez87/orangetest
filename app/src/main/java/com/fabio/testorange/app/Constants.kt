package com.fabio.testorange.app

import java.text.SimpleDateFormat
import java.util.*

const val BASE_URL = "https://api.myjson.com"

val DATE_FORMATTER: SimpleDateFormat = SimpleDateFormat("EEE M/dd/yyyy hh:mm a", Locale.US)

