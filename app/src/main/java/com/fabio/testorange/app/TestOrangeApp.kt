package com.fabio.testorange.app

import android.app.Application
import android.content.Context

class TestOrangeApp: Application() {

    companion object {
        private lateinit var instance: TestOrangeApp
        fun getAppContext(): Context = instance.applicationContext

        fun validateText(value: String?) =
            if (value.isNullOrEmpty()) "Not value." else value

        fun validateAmount(value: Float) =
            if (value < 0) "Gasto" else "Ingreso"

        fun isNegativeAmount(value: Float): Boolean{
            return (value < 0)
        }

        fun fixFee(amount: Float, fee: Float?): Float{
            return if(fee != null){
                amount + fee
            } else{
                amount
            }
        }
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }
}