package com.fabio.testorange.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fabio.testorange.app.Injection
import com.fabio.testorange.app.toast
import com.fabio.testorange.model.Transaccion
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

object RemoteRepository: Repository {

    private val api = Injection.provideOrangeApi()
    private val parentJob = Job()

    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.IO

    private val scope = CoroutineScope(coroutineContext)


    override fun getTransacciones(): LiveData<List<Transaccion>> {
        val liveData = MutableLiveData<List<Transaccion>>()
        scope.launch{
            api.getTransacciones().enqueue(object : Callback<List<Transaccion>> {
                override fun onResponse(call: Call<List<Transaccion>>?, response: Response<List<Transaccion>>?) {
                    if (response != null && response.isSuccessful) {
                        liveData.value = response.body()
                    } else {
                        toast("Error Response!")
                    }
                }

                override fun onFailure(call: Call<List<Transaccion>>, t: Throwable) {
                    // liveData.value = Result.error(ApiError.REPOS, null)
                    toast("Error!")
                }
            })
        }
        return liveData
    }

    override fun cancelAllRequests() = coroutineContext.cancel()
}