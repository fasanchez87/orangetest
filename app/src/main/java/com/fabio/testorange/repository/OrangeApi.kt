package com.fabio.testorange.repository


import com.fabio.testorange.model.Transaccion
import retrofit2.Call
import retrofit2.http.GET

interface OrangeApi {
    @GET("/bins/1a30k8")
    fun getTransacciones(): Call<List<Transaccion>>
}
