package com.fabio.testorange.repository

import androidx.lifecycle.LiveData
import com.fabio.testorange.model.Transaccion

interface Repository {
    fun getTransacciones(): LiveData<List<Transaccion>>
    fun cancelAllRequests()
}