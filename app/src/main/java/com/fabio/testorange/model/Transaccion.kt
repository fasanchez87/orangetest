package com.fabio.testorange.model

import java.util.*

data class Transaccion(val id: Int,
                       val date: String,
                       val amount: Float,
                       val fee: Float,
                       val description: String)
