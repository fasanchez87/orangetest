package com.fabio.testorange.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fabio.testorange.model.Transaccion
import com.fabio.testorange.repository.RemoteRepository
import com.fabio.testorange.repository.Repository

class TransaccionViewModel(private val repository: Repository = RemoteRepository): ViewModel() {

    fun getTransacciones(): LiveData<List<Transaccion>>{
        return repository.getTransacciones()
    }

    fun cancelRequestEventos(){
        repository.cancelAllRequests()
    }
}