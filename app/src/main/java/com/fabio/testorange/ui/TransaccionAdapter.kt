package com.fabio.testorange.ui

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fabio.testorange.R
import com.fabio.testorange.app.TestOrangeApp.Companion.fixFee
import com.fabio.testorange.app.TestOrangeApp.Companion.getAppContext
import com.fabio.testorange.app.TestOrangeApp.Companion.isNegativeAmount
import com.fabio.testorange.app.TestOrangeApp.Companion.validateAmount
import com.fabio.testorange.app.TestOrangeApp.Companion.validateText
import com.fabio.testorange.app.inflate
import com.fabio.testorange.app.round
import com.fabio.testorange.model.Transaccion
import kotlinx.android.synthetic.main.trans_row_layout.view.*


class TransaccionAdapter(private val transaccion: MutableList<Transaccion>, private val clickListener: (Transaccion) -> Unit):
    RecyclerView.Adapter<TransaccionAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.trans_row_layout))
    }

    override fun getItemCount() = transaccion.size

    fun updatePhotos(transaccion: List<Transaccion>){
        this.transaccion.clear()
        //ORDER LIST BY DATE: DEBIDO A UN ERROR DE FORMAT EN EL JSON DE RESPUESTA, ORDENAMOS POR ID
        this.transaccion.addAll(transaccion.sortedBy { it.id })
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(transaccion[position], clickListener)
    }

    inner class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        private lateinit var transaccion: Transaccion

        fun bind(transaccion: Transaccion, clickListener: (Transaccion) -> Unit){
            this.transaccion = transaccion
            itemView.description.text = validateText(transaccion.description)
            itemView.type_amount.text = validateAmount(transaccion.amount)
            //EXTENSION FUNCTION ROUND
            itemView.amount.text = fixFee(transaccion.amount, transaccion.fee).round().toString()

            if(isNegativeAmount(transaccion.amount)){
                itemView.indicator_amount.setBackgroundResource(R.color.color_minus)
                itemView.amount.setTextColor(Color.parseColor(getAppContext().getString(R.string.color_minus)))
            }
            else{
                itemView.indicator_amount.setBackgroundResource(R.color.color_add)
                itemView.amount.setTextColor(Color.parseColor(getAppContext().getString(R.string.color_add)))
            }
        }
    }
}