package com.fabio.testorange.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.fabio.testorange.R
import com.fabio.testorange.model.Transaccion
import com.fabio.testorange.viewmodel.TransaccionViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class Home : AppCompatActivity() {

    private lateinit var transaccionViewModel: TransaccionViewModel
    private val adapter = TransaccionAdapter(mutableListOf()){
        navigateToDetail(it)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        transaccionViewModel = ViewModelProviders.of(this).get(TransaccionViewModel::class.java)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter

        transaccionViewModel.getTransacciones().observe(this, Observer<List<Transaccion>> { transaccion ->
            if(transaccion != null){
                adapter.updatePhotos(transaccion)
                progressbar.visibility = View.GONE
                recycler.visibility = View.VISIBLE
            }
            else{
                toast("Error gettings events!")
            }
        })
    }

    private fun navigateToDetail(it: Transaccion) {

    }
}
